CXX = g++ -Wall -std=c++0x

CXXFLAGS = `pkg-config --cflags --libs opencv`

BUILD_DIR = ./build
SRC_DIR = ./src

TARGET = image_processing
SOURCE = $(SRC_DIR)/main.cpp \
		$(SRC_DIR)/util.cpp

TARGET:
	$(CXX) -o $(BUILD_DIR)/$(TARGET) $(SOURCE) $(CXXFLAGS)

.PHONY: clean

clean:
	rm -f *.o *~ $(BUILD_DIR)/*