#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/opencv.hpp>
#include <iostream>

using namespace cv;
using namespace std;

struct point_map
{
        int X;
        int Y;
}point_map;



int read_image(Mat *inputImageRGB, Mat *outputImageRGB, const cv::String& filename)
{
    *inputImageRGB = imread(filename, CV_LOAD_IMAGE_GRAYSCALE);   // Read the file

    //*outputImageRGB = inputImageRGB->clone();

    if(! (*inputImageRGB).data )                              // Check for invalid input
    {
        cout <<  "Could not open or find the image" << std::endl ;
        return -1;
    }

    namedWindow( "Original image", WINDOW_AUTOSIZE );// Create a window for display.
    imshow( "Original image", *inputImageRGB );                   // Show our image inside it.

    return 0;
}

void blurr_image(Mat *outputImageRGB)
{
    cv::Mat &ui = *outputImageRGB; 
    cv::Mat ui3 = cv::Mat::zeros(ui.rows, ui.cols, CV_8U); 
    int i, j, z, t; 
    
    double Number = 0;
    double Number2 = 0;

    double Distance;
    double DistanceRatio;
    double Sybiraemo = 0;
    
    double *matrix = (double *)calloc(ui.cols * ui.rows, sizeof(double));
     //// B L U R
    struct point_map BlurCentralPoint = { 200, 200 };//ui.cols/2, ui.rows/2 };
    int SizeOfBlur = 30;//percentage of width that is CLEAR (no Blur) around BlurCentralPoint
    int BlurAgression = 100; // ot 1 do 100; (Bigger than 0)
    int BlurPixelRadius = 5;// Should be Even number - min 5
    double MaxRatio;
    ////////////////

    MaxRatio = (double)MAX(BlurCentralPoint.X - ((double)SizeOfBlur * ui.cols / 100), ui.cols - BlurCentralPoint.X + ((double)SizeOfBlur * ui.cols / 100)) / ((double)SizeOfBlur * ui.cols / 100);
    for( i = 0; i < ui.rows; i++ )
    {
        for ( j = 0; j < ui.cols; j++ )
        {
            Distance = sqrt(pow(abs((double)BlurCentralPoint.X - j), 2) + pow(abs((double)BlurCentralPoint.Y - i), 2));
            if (Distance > ((double)SizeOfBlur * ui.cols / 100))
            {
                    matrix[i * ui.cols + j] = 1;
            }
            else
            {
                    DistanceRatio = Distance / ((double)SizeOfBlur * ui.cols / 100);
                    matrix[i * ui.cols + j] = 1 - ((double)BlurAgression/100 *( DistanceRatio/ MaxRatio));
                    if(matrix[i * ui.cols + j] < 0) matrix[i * ui.cols + j] = 0;
            }
        }
    }


    for( i = BlurPixelRadius/2; i < ui.rows-BlurPixelRadius/2; i++ )
    {
        for ( j = BlurPixelRadius/2; j < ui.cols-BlurPixelRadius/2; j++ )
        {
            Sybiraemo = 0;

            Number2 = ((double)(matrix[i * ui.cols + j])/(pow((double)BlurPixelRadius,2) -1 - (12 + (2*(BlurPixelRadius - 5)))));
            
            for( z = 0; z < BlurPixelRadius/2; z++)
            {
                    for( t = 0; t < BlurPixelRadius/2; t++)
                    {
                        Sybiraemo += ui.data[(i-z)*ui.cols + j-t]/2;
                        Sybiraemo += ui.data[(i-z)*ui.cols + j+t]/2;
                        Sybiraemo += ui.data[(i+z)*ui.cols + j-t]/2;
                        Sybiraemo += ui.data[(i+z)*ui.cols + j+t]/2;
                    }
            }
                
            Number2 *= Sybiraemo;
            Number = (1-matrix[i * ui.cols + j])*ui.data[i*ui.cols + j] + (int)Number2;

            if(Number > 255) 
                Number = 255;
            if(Number < 0) 
                Number = 0;
            ui3.data[i*ui.cols + j] = Number;
        }
    }

    namedWindow("Blurred image",WINDOW_AUTOSIZE);
    imshow("Blurred image",ui3);
    imwrite("al2.jpg",ui3);

}