#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <iostream>

using namespace cv;
using namespace std;

int read_image(Mat *inputImageRGBA, Mat *outputImageRGBA, const cv::String& filename);
void blurr_image(Mat *outputImageRGB);



int main( int argc, char** argv )
{
    if( argc != 2)
    {
     cout <<" Usage: display_image ImageToLoadAndDisplay" << endl;
     return -1;
    }
    Mat input_image ;
    read_image(&input_image, nullptr, argv[1]);

    blurr_image(&input_image);
    

    waitKey(0);    
    return 0;
}